<?php
    namespace Aghai\Steps\Controller\Adminhtml\Index;
    class Savecaratimages extends \Magento\Backend\App\Action{
         public function execute(){
            $data = $this->getRequest()->getParams();
            $ProductId = $data['portfolio_pid'];
            $carat = $data['carat'];
            $totalChild = 0;
            $three60 = 0;
            if(isset($data['three60']))
            {
                $three60 = 1;
            }
            $FirstImgPos = 'NIL';
            
                    $target_dir = "caratimages/";
                    $DirPath = $target_dir.$ProductId."/".$carat;
                    if(!file_exists($target_dir.$ProductId)){
                        mkdir($target_dir.$ProductId, 0777);
                    }
                      # print_r($data);print_r($_FILES);die;
                      for ($i=0; $i <count($_FILES['upload_caratimg']['name']) ; $i++) { 
                            if($_FILES['upload_caratimg']['name'][$i]!=''):  
                                  if($i<10){               
                                     $target_file = $DirPath .'/caratimg_0'.$i.'.png';
                                  }
                                  if($i>9){               
                                     $target_file = $DirPath .'/caratimg_'.$i.'.png';
                                  }
                                   if(!file_exists($DirPath)){
                                       mkdir($DirPath, 0777);
                                       #hell_exec('mkdir rm -rf '.$target_file);
                                   } 
                                    if (move_uploaded_file($_FILES["upload_caratimg"]["tmp_name"][$i], $target_file)) {
                                         if($FirstImgPos == 'NIL'){
                                            $FirstImgPos = $i;
                                         }  $totalChild++;
                                       #  $this->_messageManager->addSuccess(__("360 Portfolio has been uploaded for ProductId:".$ProductId."  ChildID:".$ChildId));
                                    } else {
                                        echo "Sorry, there was an error uploading your file.";
                                    }                                  
                            endif;
                      }             
            
              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $PortfolioModel = $Objectinstance->create('Aghai\Steps\Model\Caratimages');
              $PortfolioModel->setPid($ProductId)->setCarat($carat)->setTotalchilds($totalChild)->setFirstImage($DirPath.'/caratimg_0.png')->setThree60($three60)->save();

              $resultRedirect = $this->resultRedirectFactory->create();
              $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
              return $resultRedirect;
        }
    }
?>