<?php
    namespace Aghai\Steps\Controller\Adminhtml\Index;
    class Index extends \Magento\Backend\App\Action{
         public function execute(){
            $data = $this->getRequest()->getParams();
            $ProductId = $data['portfolio_pid'];
            $ChildId = $data['childId'];
            $totalChild = 0;
            $three60 = 0;
            if(isset($data['three60']))
            {
                $three60 = 1;
            }
            $FirstImgPos = 'NIL';
            $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
                    $target_dir = "portfolio360images/";
                    $DirPath = $target_dir.$ProductId."/".$ChildId;
                    if(!file_exists($target_dir.$ProductId)){
                        mkdir($target_dir.$ProductId, 0777);
                    }
                      # print_r($data);print_r($_FILES);die;
                      for ($i=0; $i <count($_FILES['upload_portfolioimg']['name']) ; $i++) { 
                            if($_FILES['upload_portfolioimg']['name'][$i]!=''): 
                                  if($i<10){
                                     $target_file = $DirPath .'/portfolio_0'.$i.'.png';
                                     $resizedFile = $DirPath .'/resize/resized_portfolio_0'.$i.'.png';
                                  }
                                  if($i>9){
                                     $target_file = $DirPath .'/portfolio_'.$i.'.png';
                                     $resizedFile = $DirPath .'/resize/resized_portfolio_'.$i.'.png';
                                  }                
                                  
                                   if(!file_exists($DirPath)){
                                       mkdir($DirPath, 0777);
                                       mkdir($DirPath.'/resize', 0777);
                                       #hell_exec('mkdir rm -rf '.$target_file);
                                   } 
                                    if (move_uploaded_file($_FILES["upload_portfolioimg"]["tmp_name"][$i], $target_file)) {
                                         if($FirstImgPos == 'NIL'){
                                            $FirstImgPos = $i;
                                         }  $totalChild++;
                                          $imagehelpers = $Objectinstance->create('Aghai\Steps\Helper\Image'); 
                                          $imagehelpers->smart_resize_image($target_file , null, 500 ,500 , false , $resizedFile , false , false ,100 , true);
                                       #  $this->_messageManager->addSuccess(__("360 Portfolio has been uploaded for ProductId:".$ProductId."  ChildID:".$ChildId));
                                    } else {
                                        echo "Sorry, there was an error uploading your file.";
                                    }                                  
                            endif;
                      }             
            
              
              $PortfolioModel = $Objectinstance->create('Aghai\Steps\Model\Portfolio');
              $PortfolioModel->setPid($ProductId)->setChildId($ChildId)->setTotalchilds($totalChild)->setFirstImg($DirPath.'/resize/resized_portfolio_00.png')->setThree60($three60)->save();

              $resultRedirect = $this->resultRedirectFactory->create();
              $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
              return $resultRedirect;
        }
    }
?>