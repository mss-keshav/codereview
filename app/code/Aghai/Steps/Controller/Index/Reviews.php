<?php
namespace Aghai\Steps\Controller\Index;

class Reviews extends \Magento\Framework\App\Action\Action
{
 protected $_pageFactory;
  protected $resultPageFactory;
 public function __construct(
  \Magento\Framework\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory,
  \Magento\Framework\View\Result\PageFactory $pageFactory)
 {
  $this->_pageFactory = $pageFactory;
  $this->resultPageFactory = $resultPageFactory;
  return parent::__construct($context);
 }

 public function execute()
 {	 /*$this->_pageFactory->getConfig()->getTitle()->set(__('Checkout'));
 	return $this->_pageFactory->create();*/
 	$resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Reviews'));
        return $resultPage;
 }
}