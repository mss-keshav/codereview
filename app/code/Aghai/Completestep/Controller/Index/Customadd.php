<?php
 
namespace Aghai\Completestep\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
 
class Customadd extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_cart;
    protected $productRepository;
    public function __construct(Context $context,\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, PageFactory $resultPageFactory,\Magento\Checkout\Model\Cart $_cart) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cart = $_cart;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }
 
    public function execute()
    {
            $pid = $this->getRequest()->getParam('pid');
            $data['selectedAval'] = $this->getRequest()->getParam('selectedAval');
               # print_r($data); 
              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $resultRedirect = $this->resultRedirectFactory->create();
              /*   Array ( [uenc] => aHR0cDovLzUyLjMwLjc1LjIwOC9lbXJhZGFkZXYyLzE0ay13aGl0ZS1nb2xkLXRocmVlLXN0b25lLXN1bmJ1cnN0LXJpbmcuaHRtbA,, [product] => 285 [selected_configurable_option] => 440 [related_product] => [form_key] => EXMiSqTVAuuA448W [super_attribute] => Array ( [319] => 870 ) [checkoutbtnflag] => true )*/
                $ProductObj = $Objectinstance->create('Magento\Catalog\Model\Product');     
                $SettingProd = $ProductObj->load($pid); 
                // $this->addProd2($pid);
                 $CatUrl= $this->_redirect->getRefererUrl();
                  if($SettingProd){
                        $categories = $SettingProd->getCategoryIds(); /*will return category ids array */
                        foreach($categories as $category){
                            $cat = $Objectinstance->create('Magento\Catalog\Model\Category')->load($category);
                            $CatUrl= $cat->getUrl();
                        }
                    }
                 $productTypeInstance = $SettingProd->getTypeInstance();
                $usedProducts = $productTypeInstance->getUsedProducts($SettingProd);
                $selected_configurable_option = '';

                foreach ($usedProducts  as $child) {
                   # echo '<br/> Id: '.$child->getId().'  Name: '.$child->getName().'  Option:'.$child->getData('dyo_rings_metal').'<br/>';
                             $ChildProductsIds[] = $child->getId();
                             $ChildProductsConfigOptions[] =$child->getData('dyo_rings_metal');
                             #$selected_configurable_option = $child->getId();
                             if($child->getData('dyo_rings_metal')==$data['selectedAval']){
                                $selected_configurable_option =$child->getData('dyo_rings_metal');
                                $SelectedChildProductId = $child->getId();
                             }
                             #echo 'Childs:: '.$child->getId().'  ==> '.$child->getData('dyo_rings_metal').' <==> '.$data['selectedAval'].'<br/>';
                } 
                if($data['selectedAval']==0){
                    $selected_configurable_option = $ChildProductsConfigOptions[0];
                    $SelectedChildProductId = $ChildProductsIds[0];
                } 
                $this->addProd2($SelectedChildProductId);
                //die;$selected_configurable_option =36;
             //get products instances
               # print_r($data);die;
               /*$SettingProd ='';$StoneProd ='';   
                switch ($data['btnId']) {
                               case 'A':
                                   $this->addProd2($SelectedChildProductId);
                                   $this->addProd2($data['prod2']);
                                   $resultRedirect->setPath('checkout/cart');
                                   break;
                               case 'B':
                                   $resultRedirect->setPath($this->_redirect->getRefererUrl());
                                   break;
                                case 'C':
                                   $this->addProd2($SelectedChildProductId);
                                   $this->addProd2($data['prod2']);
                                   $resultRedirect->setPath('checkout');
                                   break;
                               default:
                                   $resultRedirect->setPath('checkout/cart');
                                   break;
             } */ 
               $resultRedirect->setPath($CatUrl); 
               return $resultRedirect;
            
    }
    protected function addProd2($prod2){
                $params = array();
                $params['qty'] = 1;
                $Objectinstance1 = \Magento\Framework\App\ObjectManager::getInstance();  
                $cart = $Objectinstance1->create('Magento\Checkout\Model\Cart');                 
                $productObj2 = $Objectinstance1->create('Magento\Catalog\Model\Product');      
                $StoneProd = $productObj2->load($prod2);
                $cart->addProduct($StoneProd, $params);
                $cart->save();
    }
}
