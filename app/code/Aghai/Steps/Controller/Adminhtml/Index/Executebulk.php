<?php
namespace Aghai\Steps\Controller\Adminhtml\Index;

class Executebulk extends \Magento\Backend\App\Action{
 protected $_pageFactory;
 public function __construct(
  \Magento\Framework\App\Action\Context $context,
  \Magento\Framework\View\Result\PageFactory $pageFactory)
 {
      $this->_pageFactory = $pageFactory;
      return parent::__construct($context);
 }


 public function execute(){     
  $data = $this->getRequest()->getParams();
  // recevive data categoryID & csv file
    $catType = 0;
    $data = $this->getRequest()->getParams();
    if(strlen($data['product_type'])>2){
        $catType = $data['product_type'];
    }
//parse csv file and store into array 
    $ProductsDataFromCSV = '';
    if($_FILES["file"]){
        $setting_shape = array(964=>'Round',917=>'Princess',927=>'Cushion',965=>'Emerald',966=>'Pear',967=>'Oval',968=>'Heart',918=>'Radiant',926=>'Asscher',919=>'Baguette',920=>'Triangle',921=>'Trilliant',922=>'Taper Baguette',923=>'Square',924=>'Hexagon',925=>'Octagon',928=>'Star',929=>'Half Moon',930=>'Spring',931=>'Gabrielle',932=>'Calfshead',933=>'Bullet',934=>'Kite',935=>'Shield',936=>'Trapeze',937=>'Briolette',938 =>'Flanders',939=>'European',940=>'Regentv',941=>'Rose',942=>'Lozenge',943=>'Horse Head',944=>'Bead',945=>'Christmas Tree',946=>'Brilliant',947=>'Lady',948=>'Lady Heart',949=>'Webcut',950=>'Forever',951=>'Corona',952=>'Single',953=>'Old',954=>'Special',955=>'Rough',969=>'Marquise');

        $dyo_rings_style = array(191=>'Solitaire',197=>'Halo',192=>'Pavé',194=>'Three-Stone',196=>'Vintage',199=>'Side stone',198=>'Bridal Set',195=>'Tension',200=>'Special-collection',193=>'Channel-Set');

        $dyo_rings_metal = array(202=>'White Gold 14k',203=>'Yellow Gold 14k',201=>'Rose Gold 14k',206=>'Platinum',204=>'White Gold 18k',205=>'Yellow Gold 18k');

        $dyo_rings_average_width = array(217=>'2.2-1.7 mm');

        $dyo_rings_length = array(218=>'1.1-1.9');

        $steps = array(869=>'Setting',870=>'stone',871=>'n/a');

        $dyo_rings_rhodium_finish = array(225=>'Yes',226=>'No');

        $dyo_rings_side_stones_total_we = array(369=>'0.62 ctw');

        $dyo_rings_side_stones_avg_cut = array(370=>'Very Good');

        $dyo_rings_side_stones_avg_colo = array(371=>'F- G');

        $dyo_rings_if_side_stones_are_f = array(372=>'Yellow',373=>'Pink',374=>'Purple',375=>'Red',376=>'Blue',377=>'Green',378=>'Orange',379=>'Brown',380=>'Black',381=>'Gray');

        $dyo_rings_side_stones_avg_inte = array(382=>'fancy light',383=>'fancy',384=>'fancy intense',385=>'fancy vivid');

        $dyo_rings_side_stones_avg_clar = array(386=>'fancy light');
        
        $filename = $_FILES["file"]["tmp_name"];
         $f = fopen($filename, "r");
         $incrementer = 0;
        while (($line = fgetcsv($f)) !== false) {
                if($incrementer>0):
                      $sku[] = $line[1];
                      $CSV_name = $line[0];
                      $CSV_sku =$line[1];
                      $CSV_price = $line[2];
                      $CSV_qty = $line[3];
                      $CSV_category_ids = $line[4];
                      $CSV_country_of_manufacture = $line[5];
                      $CSV_francy_description_name = $line[6];
                      $CSV_setting_shape = $line[7];
                      $CSV_dyo_rings_style = $line[8];
                      $CSV_dyo_rings_metal = $line[9];
                      $CSV_dyo_rings_average_width = $line[10];
                      $CSV_dyo_rings_length = $line[11];
                      $CSV_dyo_rings_rhodium_finish = $line[12];
                      $CSV_supplier = $line[13];
                      $CSV_shipping_days = $line[14];
                      $CSV_steps = $line[15];
                      $CSV_dyo_rings_side_stones_total_we = $line[16];
                      $CSV_dyo_rings_side_stones_avg_cut = $line[17];
                      $CSV_dyo_rings_side_stones_avg_colo = $line[18];
                      $CSV_dyo_rings_if_side_stones_are_f = $line[19];
                      $CSV_dyo_rings_side_stones_avg_inte = $line[20];
                      $CSV_dyo_rings_side_stones_avg_clar = $line[21];
                      $CSV_description = $line[22];
                      $CSV_short_description = $line[23];
                      //create simple products
                      $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
                      $simple_product = $Objectinstance->create('\Magento\Catalog\Model\Product');
                      $simple_product->setSku($CSV_sku);
                      $simple_product->setName($CSV_name);
                      $simple_product->setAttributeSetId(4);
                      $simple_product->setStatus(1);
                      $simple_product->setTypeId('simple');
                      $simple_product->setPrice($CSV_price);
                      $simple_product->setWebsiteIds(array(1));
                      $simple_product->setCategoryIds(array(14));
                      $simple_product->setStockData(array(
                          'use_config_manage_stock' => 0, //'Use config settings' checkbox
                          'manage_stock' => 1, //manage stock
                          'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                          'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
                          'is_in_stock' => 1, //Stock Availability
                          'qty' => $CSV_qty//qty
                          )
                      );
                      //simple                                  
                      $simple_product->setShippingDays($CSV_shipping_days);
                      $simple_product->setSupplier($CSV_supplier);
                      $simple_product->setFrancyDescriptionName($CSV_francy_description_name);
                      $simple_product->setCountryOfManufacture($CSV_country_of_manufacture);
                      $simple_product->setDescription($CSV_description);
                      $simple_product->setShortDescription($CSV_short_description);
//config
$simple_product->setDyoRingsSideStonesAvgClar(array_search($CSV_dyo_rings_side_stones_avg_clar,$dyo_rings_side_stones_avg_clar));
$simple_product->setSettingShape(array_search($CSV_setting_shape,$setting_shape));
$simple_product->setDyoRingsRingsStyle(array_search($CSV_dyo_rings_style,$dyo_rings_style));
$simple_product->setDyoRingsMetal(array_search($CSV_dyo_rings_metal,$dyo_rings_metal));
$simple_product->setDyoRingsAverageWidth(array_search($CSV_dyo_rings_average_width,$dyo_rings_average_width));
$simple_product->setDyoRingsLength(array_search($CSV_dyo_rings_length,$dyo_rings_length));
$simple_product->setDyoRingsRhodiumFinish(array_search($CSV_dyo_rings_rhodium_finish,$dyo_rings_rhodium_finish));
$simple_product->setSteps(array_search($CSV_steps,$steps));
$simple_product->setDyoRingsSideStonesTotalWe(array_search($CSV_dyo_rings_side_stones_total_we,$dyo_rings_side_stones_total_we));
$simple_product->setDyoRingsSideStonesAvgCut(array_search($CSV_dyo_rings_side_stones_avg_cut,$dyo_rings_side_stones_avg_cut));
$simple_product->setDyoRingsSideStonesAvgColo(array_search($CSV_dyo_rings_side_stones_avg_colo,$dyo_rings_side_stones_avg_colo));
$simple_product->setDyoRingsIfSideStonesAreF(array_search($CSV_dyo_rings_if_side_stones_are_f,$dyo_rings_if_side_stones_are_f));
$simple_product->setDyoRingsSideStonesAvgInte(array_search($CSV_dyo_rings_side_stones_avg_inte,$dyo_rings_side_stones_avg_inte));
$simple_product->save();


                            endif;
                            $incrementer++;                      
                    }

                    fclose($f);
                }elseif (!$_FILES["file"]) {
                   echo 'CSV not received. Try again.';die;
                }

            //iterate array in loop starts
                 // insert each product info as simple product
            //iteration end



            //store created products IDS in array


            //rerurn array of created prod iDs
        }
    }
?>