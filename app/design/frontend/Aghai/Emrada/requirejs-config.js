var config = {
    // When load 'requirejs' always load the following files also
    deps: [
    "js/main"
    ],
    
    paths: {
        "jquery.bootstrap"  : "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min",
        "jquery.boxslider"  : "https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.min",
        "select2"           : "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min"
    },
    shim: {
        'jquery.bootstrap': {
            'deps': ['jquery']
        },
        'jquery.boxslider': {
            'deps': ['jquery']
        },
        'select2': {
            'deps': ['jquery']
        }
    }
};