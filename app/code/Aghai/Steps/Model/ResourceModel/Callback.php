<?php

namespace Aghai\Steps\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Callback extends AbstractDb
{
    public function _construct()
    {
        $this->_init('callback', 'id');
    }
}