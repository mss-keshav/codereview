<?php

namespace Aghai\Steps\Model\ResourceModel\Mapping;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
     
    public function _construct()
    {
        $this->_init('Aghai\Steps\Model\Mapping', 'Aghai\Steps\Model\ResourceModel\Mapping');
    }
}