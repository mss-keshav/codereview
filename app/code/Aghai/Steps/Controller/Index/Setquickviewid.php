<?php
namespace Aghai\Steps\Controller\Index;
class Setquickviewid extends \Magento\Framework\App\Action\Action
{
     protected $_pageFactory;
     protected $_productloader; 
     protected $Prohelper;
     protected $Comparhelper;
     public function __construct(
      \Magento\Catalog\Model\ProductFactory $_productloader,
      \Magento\Framework\App\Action\Context $context,
      \Magento\Catalog\Helper\Product $Prohelper,
      \Magento\Catalog\Helper\Product\Compare $Comparhelper,
      \Magento\Framework\View\Result\PageFactory $pageFactory)
      {
          $this->_pageFactory = $pageFactory;
          $this->_Prohelper = $Prohelper;
          $this->_Comparhelper = $Comparhelper;
          $this->_productloader = $_productloader;
          //$this->FacHelper = $FacHelper;
          return parent::__construct($context);
      }
      public function getLoadProduct($id)
      {
        return $this->_productloader->create()->load($id);
      }
      public function execute()
      { 
        $pid=0;
        $pid = $this->getRequest()->getParam('pid');
        $_product=$this->getLoadProduct($pid);
        $images = $_product->getMediaGalleryImages();
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $SessionObj = $objectManager->create('Magento\Customer\Model\SessionFactory')->create();        
        $appState = $objectManager->get('\Magento\Framework\App\State');           
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $currency = $objectManager->get('\Magento\Directory\Model\Currency');
        $store = $storeManager->getStore();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $BaseUrl = $storeManager->getStore()->getBaseUrl();
        $categories = $_product->getCategoryIds(); /*will return category ids array */
        foreach($categories as $category){
            $Objectinstance = $objectManager;
            $cat = $Objectinstance->create('Magento\Catalog\Model\Category')->load($category);
            $CatUrl= $cat->getUrl();
            $CurrentCat = $cat->getId();
            $settingArr=array(36,37,38,39,40,14);
            $readytoship = array(2,27,28,29,30,18,19);
        }
        $SessionObj->setQuickviewId($pid);
        /*if(in_array($CurrentCat, $settingArr)){
        $buttonTitle =('Choose This Setting');
        }
        if(in_array($CurrentCat, $weddingArr)){
        $buttonTitle =('Add to cart');
        }*/
        /*review reating*/
        $RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($pid);
        //echo $_product->getId();
        echo '<script>
        function choosethis($pid){
        var chooseUrl = "'.$BaseUrl.'steps/index/choose?pid="+$pid+"&selectedAval=0";           
        window.location = chooseUrl;
        }  
        </script>
        <script type="text/x-magento-init">
            {
                "*": {
                    "Magento_Ui/js/core/app": {
                        "components": {
                            "wishlist": {
                                "component": "Magento_Wishlist/js/view/wishlist"
                            }
                        }
                    }
                }
            }
        </script>
        <div class="clearfix pad-dt">
            <div class="col-sm-7 text-center">
                <div class="gallery-quickview">
                    <div class="loader-overlay"><span class="loader-img"></span></div>
                    <ul class="list-unstyled quick-view-thumb-slider quick-left">';
                        $a=0;
                        foreach($images as $child){
                        echo '<li class="slider_img mrg-db"><a data-slide-index="'.$a.'" href=""><img class="img-responsive" src="'.$child->getUrl().'" ></a></li>';
                        $a++;
                        } 
                    echo '</ul>
                    <ul class="quick-view-slider">';
                        foreach($images as $child){
                        echo '<li class="slide"><img class="img-responsive" src="'.$child->getUrl().'" ></li>';
                        }
                        echo '
                    </ul>
                    <ul id="" class="list-inline quick-view-thumb-slider mrg-dt">';
                        $j=0;
                        foreach($images as $child){
                        echo   '<li class="slider_img"><a data-slide-index="'.$j.'" href=""><img class="img-responsive" src="'.$child->getUrl().'" ></a></li>';
                        $j++;
                        }
                    echo '</ul>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="product-information text-left">
                    <h4 class="text-uppercase product-super-title" id="Prod-superHead-quickview"></h4>
                    <h3 class="product-title text-uppercase">
                    <span class="base" data-ui-id="page-title-wrapper">'.$_product->getName().'</span>
                    </h3>
                    <div class="setting-price">
                        <div class="price-box price-final_price" data-role="priceBox" data-product-id="1">
                            <span class="price">'.$currency->getCurrencySymbol().' '.$_product->getFinalPrice().'</span>
                        </div>
                        <div class="price-text">Setting price</div>
                    </div>
                    <div class="product-reviews-summary">';
                    if ($RatingOb->getCount()>0) {
                        echo '<div class="quickviw-review">
                                <span class="product-score">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </span><div class="reviews-actions">
                                    <a target="_blank" class="action view btn btn-link" href="'.$BaseUrl.'steps/index/reviews?pid='.$_product->getId().'">
                                        <span itemprop="reviewCount">'.$RatingOb->getCount().' Reviews</span>
                                    </a>
                                </div>
                            </div>'; 
                        } 
                        echo '<div class="product attribute overview">
                            <div class="value" itemprop="description">'.$_product->getShortDescription().'</div>
                        </div>';
                        if ($_product->getData('type_id') == "configurable"):
                        
	                        //get the configurable data from the product
	                        $config = $_product->getTypeInstance(true);
	                        //loop through the attributes
	                        foreach($config->getConfigurableAttributesAsArray($_product) as $attributes):
                                echo '<div class="input-box configurable control mrg-db " id="choose-an-option"><select name="super_attribute['.$attributes["attribute_id"] .']" id="attribute'.$attributes["attribute_id"] .'">';
                                foreach($attributes["values"] as $values)
                                    {
                                        echo "<option>".$values['label']."</option>";
                                    };echo '</select><span class="select-style" >Choose an Option..</span></div>';
	                        endforeach;
                        endif;
                        if(in_array($CurrentCat, $settingArr)){
                        echo '<input type="button" class="primary btn btn-xxl btn-block btn-dark" value="Choose This Setting" onclick="choosethis('.$pid.');" disabled="disabled">';
                        }
                        if(in_array($CurrentCat, $readytoship)){
                            $onclick = "window.location.href='".$BaseUrl."completestep/index/customadd?pid=".$pid."&selectedAval=0'";
                        echo '<input type="button" class="primary btn btn-xl btn-block btn-dark" value="Add To Cart" onclick="'.$onclick.'" disabled="disabled"/>';
                        }
                        echo '<div class="product-addto-links">';
                         //   if ($this->getWishlistHelper()->isAllow());
                                echo'<a href="'.$BaseUrl.'completestep/index/customaddwishlist?pid='.$pid.'&selectedAval=0"
                                   class="action towishlist"
                                   title=""
                                   aria-label=""
                                  
                                   role="button">
                                    <i class="em em-heart-plus mrg-xr" aria-hidden="true"></i> save to favourites</a>
                                </a>';
                             echo'
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix border-top mrg-gt">
            <div class="col-sm-8">
                <div class="friend-opinion ">
                    <ul class="list-inline social">
                        <li>
                            <h5>Ask a friend for opinion</h5>
                        </li>
                        <li>
                            <ul class="list-inline social-icons ">
                                <li>
                                    <a target="_blank" class="action mailto friend text-sec" href="'.$this->_Prohelper->getEmailToFriendUrl($_product).'">
                                        <i class="em em-e-mail mrg-xr" aria-hidden="true"></i>E-mail
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-sec" href="https://www.facebook.com/sharer/sharer.php?u='.urlencode($_product->getProductUrl()).'&t='.urlencode($_product->getName()).'" onclick="javascript:window.open(this.href,"","width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes");return false">
                                        <i class="em em-facebook mrg-xr" aria-hidden="true"></i>Facebook
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-sec" href="http://twitter.com/home/?status='.urlencode($_product->getProductUrl()).'" onclick="javascript:window.open(this.href,"","width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes");return false">
                                        <i class="em em-twitter mrg-xr" aria-hidden="true"></i>Twitter
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-sec" href="http://web.whatsapp.com/home/?status='.urlencode($_product->getProductUrl()).' '.(urlencode($_product->getName())).'" onclick="javascript:window.open(this.href,"","width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes");return false">
                                        <i class="em em-whatsapp mrg-xr" aria-hidden="true"></i>Whatsapp
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="ship-returns text-center text-uppercase">
                    <i class="fa fa-flag-o mrg-xr" aria-hidden="true"></i>
                    <a target="_blank" href="free-shipping-and-returns">
                        <span class="sec-col-under"> free shipping </span><span class="amp-symbol"> &amp; </span><span class="sec-col-under">free returns</span>
                    </a>
                </div>
            </div>
        </div>';echo '.';die;
    }   
}
?>