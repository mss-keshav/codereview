<?php
namespace Aghai\Steps\Controller\Index;

class Adssave extends \Magento\Framework\App\Action\Action
{
    
     protected $_pageFactory;
     protected $messageManager;
     public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\Message\ManagerInterface $messageManager,
      \Magento\Framework\View\Result\PageFactory $pageFactory)
     {
          $this->_pageFactory = $pageFactory;
          $this->_messageManager = $messageManager;
          return parent::__construct($context);
     }

     public function execute()
     {      
          $data = $this->getRequest()->getParams();
          $catid = $data['categoryid'];
          $positions ='';$images ='';
           // upload files
           $target_dir = "adsimages/";
          # print_r($data);print_r($_FILES);die;
          for ($i=0; $i <count($data['position']) ; $i++) { 
                if($data['position'][$i]>0&&$_FILES['pictures']['name'][$i]!=''):                 
                      $target_file = $target_dir .'adsimg_'.$catid.'_'.$data['position'][$i].'.png';
                       if(file_exists($target_file)){
                          shell_exec('rm -rf '.$target_file);
                       }
                       if (move_uploaded_file($_FILES["pictures"]["tmp_name"][$i], $target_file)) {
                             $this->_messageManager->addSuccess(__("The file  has been uploaded.".'  adsimg_'.$catid.'_'.$data['position'][$i].'.png'));   
                              $positions[]=$data['position'][$i]; $images[] = 'adsimg_'.$catid.'_'.$data['position'][$i].'.png';
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                      
                endif;
          }
          $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
          $adsModel = $Objectinstance->create('Aghai\Steps\Model\Ads');
          $adsModel->setCatid($data['categoryid'])->setPosition(serialize($positions))->setPics(serialize($images))->save();
          $resultRedirect = $this->resultRedirectFactory->create();
          $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
          return $resultRedirect;
     }     
            
}
 