<?php

namespace Aghai\Steps\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Portfolio extends AbstractDb
{
    public function _construct()
    {
        $this->_init('portfolioimages', 'entity_id');
    }
}