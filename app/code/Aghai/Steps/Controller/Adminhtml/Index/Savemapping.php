<?php
    namespace Aghai\Steps\Controller\Adminhtml\Index;
    class Savemapping extends \Magento\Backend\App\Action{
         public function execute(){
            //receive data
            $data = $this->getRequest()->getParams();
            $pid = $data['product_pid']; 
            $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
            $Mapping = $Objectinstance->create('Aghai\Steps\Model\Mapping');
            
            $Mapping->setPid($pid)->setStype(0)->setAttrCode('stone_cut_shape_of_stone')->setSelectedvalues(serialize($data['stone_cut_shape_of_stone']))->save();
            $Mapping->unsetData();
            $Mapping->setPid($pid)->setStype(0)->setAttrCode('stone_carat_carat_weight')->setSelectedvalues(serialize(array($data['stone_carat_carat_weight_min'],$data['stone_carat_carat_weight_max'])))->save();
            $Mapping->unsetData();
             //better size better quality
             if(isset($data['bsbq'])){
                 $Mapping->setPid($pid)->setStype(6)->setAttrCode('bsbq')->setSelectedvalues($data['bsbq'])->save();
                $Mapping->unsetData();
             }
            //colorless ***************************************************************************
              if(isset($data['diamond_colorless_color'])){
                 $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_color_color_of_stone')->setSelectedvalues(serialize($data['diamond_colorless_color']))->save();
                 $Mapping->unsetData();
              }
              if(isset($data['diamond_colorless_cut'])){
                 $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_make_cut_grade')->setSelectedvalues(serialize($data['diamond_colorless_cut']))->save();
                 $Mapping->unsetData();
              }
              if(isset($data['diamond_colorless_clarity'])){
                 $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_clarity_clarity_of_stone')->setSelectedvalues(serialize($data['diamond_colorless_clarity']))->save();
                 $Mapping->unsetData();
              }
              if(isset($data['diamond_colorless_polish'])){
                 $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_polish')->setSelectedvalues(serialize($data['diamond_colorless_polish']))->save();
                 $Mapping->unsetData();
              }
              if(isset($data['diamond_colorless_symmetry'])){
                 $Mapping->setPid($pid)->setStype(1)->setAttrCode('store_symmetry')->setSelectedvalues(serialize($data['diamond_colorless_symmetry']))->save();
                 $Mapping->unsetData();
              }
              if(isset($data['diamond_colorless_fluorescence'])){
                 $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_grading_lab')->setSelectedvalues(serialize($data['diamond_colorless_fluorescence']))->save();
                 $Mapping->unsetData();
              }
              if(isset($data['diamond_colorless_certificate'])){
                 $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_fluorescence_intensity')->setSelectedvalues(serialize($data['diamond_colorless_certificate']))->save();
                 $Mapping->unsetData();
              }
              if(isset($data['diamond_depth_min'])){
              $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_total_depth')->setSelectedvalues(serialize(array($data['diamond_depth_min'],$data['diamond_depth_max'])))->save();
              $Mapping->unsetData();
              }
              if(isset($data['diamond_table_min'])){
              $Mapping->setPid($pid)->setStype(1)->setAttrCode('stone_table_width')->setSelectedvalues(serialize(array($data['diamond_table_min'],$data['diamond_table_max'])))->save();
              $Mapping->unsetData();
              }
            //fancy*****************************************************************************************
             if(isset($data['diamond_fancy_color'])){
                 $Mapping->setPid($pid)->setStype(2)->setAttrCode('stone_natural_fancy_color')->setSelectedvalues(serialize($data['diamond_fancy_color']))->save();
                 $Mapping->unsetData();
             }
             if(isset($data['diamond_fancy_cut'])){
                 $Mapping->setPid($pid)->setStype(2)->setAttrCode('stone_make_cut_grade')->setSelectedvalues(serialize($data['diamond_fancy_cut']))->save();
                 $Mapping->unsetData();
             }

            //gemstone****************************************************************************************
             if(isset($data['diamond_gemstone_color'])){
                 $Mapping->setPid($pid)->setStype(3)->setAttrCode('stone_gemstone_color')->setSelectedvalues(serialize($data['diamond_gemstone_color']))->save();
                 $Mapping->unsetData();
             }             
            //labcolorless***********************************************************************************
                    /*  if(isset($data['diamond_labcolorless_color'])){
                         $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_color_color_of_stone')->setSelectedvalues(serialize($data['diamond_labcolorless_color']))->save();
                         $Mapping->unsetData();
                      }
                      if(isset($data['diamond_labcolorless_cut'])){
                         $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_make_cut_grade')->setSelectedvalues(serialize($data['diamond_labcolorless_cut']))->save();
                         $Mapping->unsetData();
                      }
                      if(isset($data['diamond_labcolorless_clarity'])){
                         $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_clarity_clarity_of_stone')->setSelectedvalues(serialize($data['diamond_labcolorless_clarity']))->save();
                         $Mapping->unsetData();
                      }
                      if(isset($data['diamond_labcolorless_polish'])){
                         $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_polish')->setSelectedvalues(serialize($data['diamond_labcolorless_polish']))->save();
                         $Mapping->unsetData();
                      }
                      if(isset($data['diamond_colorless_symmetry'])){
                         $Mapping->setPid($pid)->setStype(4)->setAttrCode('store_symmetry')->setSelectedvalues(serialize($data['diamond_labcolorless_symmetry']))->save();
                         $Mapping->unsetData();
                      }
                      if(isset($data['diamond_labcolorless_fluorescence'])){
                         $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_grading_lab')->setSelectedvalues(serialize($data['diamond_labcolorless_fluorescence']))->save();
                         $Mapping->unsetData();
                      }
                      if(isset($data['diamond_labcolorless_certificate'])){
                         $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_fluorescence_intensity')->setSelectedvalues(serialize($data['diamond_labcolorless_certificate']))->save();
                         $Mapping->unsetData();
                      }
                       if(isset($data['diamond_labdepth_min'])){
                      $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_total_depth')->setSelectedvalues(serialize(array($data['diamond_labdepth_min'],$data['diamond_labdepth_max'])))->save();
                      $Mapping->unsetData();
                      }
                       if(isset($data['diamond_labtable_min'])){
                      $Mapping->setPid($pid)->setStype(4)->setAttrCode('stone_table_width')->setSelectedvalues(serialize(array($data['diamond_labtable_min'],$data['diamond_labtable_max'])))->save();
                      $Mapping->unsetData();
                      }*/
            //labfancy**************************************************************************************
           /* if(isset($data['diamond_labfancy_color'])){
                 $Mapping->setPid($pid)->setStype(5)->setAttrCode('stone_natural_fancy_color')->setSelectedvalues(serialize($data['diamond_labfancy_color']))->save();
                 $Mapping->unsetData();
             }
             if(isset($data['diamond_labfancy_cut'])){
                 $Mapping->setPid($pid)->setStype(5)->setAttrCode('stone_make_cut_grade')->setSelectedvalues(serialize($data['diamond_labfancy_cut']))->save();
                 $Mapping->unsetData();
             }*/

            //redirect to product edit page
             $resultRedirect = $this->resultRedirectFactory->create();
             $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
              return $resultRedirect;
        }
    }