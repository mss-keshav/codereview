<?php
 
namespace Aghai\Completestep\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
 
class Customrtsadd extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_cart;
    protected $productRepository;
    public function __construct(Context $context,\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, PageFactory $resultPageFactory,\Magento\Checkout\Model\Cart $_cart) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cart = $_cart;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }
 
    public function execute()
    {
            $pid = $this->getRequest()->getParam('pid');                
              $resultRedirect = $this->resultRedirectFactory->create();
               $this->addProd2($pid);             
               $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
               return $resultRedirect;
            
    }
    protected function addProd2($prod2){
                $params = array();
                $params['qty'] = 1;
                $Objectinstance1 = \Magento\Framework\App\ObjectManager::getInstance();  
                $cart = $Objectinstance1->create('Magento\Checkout\Model\Cart');                 
                $productObj2 = $Objectinstance1->create('Magento\Catalog\Model\Product');      
                $StoneProd = $productObj2->load($prod2);
                $cart->addProduct($StoneProd, $params);
                $cart->save();
    }
}
