<?php

namespace Aghai\Steps\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Caratimages extends AbstractDb
{
    public function _construct()
    {
        $this->_init('caratimages', 'entity_id');
    }
}