<?php

namespace Aghai\Steps\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

class Portfolio extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Aghai\Steps\Model\ResourceModel\Portfolio::class);
    }
    
}