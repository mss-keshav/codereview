<?php
namespace Aghai\Steps\Block\Adminhtml;
 
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
 
class Adsfun extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
   # protected $_template = 'product/edit/mapping.phtml';
    protected $HelperBackend;
    protected $urlBuider;
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;
 
    public function __construct(
        Context $context,\Magento\Backend\Helper\Data $HelperBackend,
        #\Magento\Framework\UrlInterface $urlBuilder,
        Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        $this->HelperBackend = $HelperBackend;
        #$this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }
    public function getAdminUrl()
    {
       return  $this->HelperBackend->getHomePageUrl();
    }
     public function getChilds($Id){
           $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
           $subcategory = $Objectinstance->create('Magento\Catalog\Model\Category')->load($Id);  
           $categoryFactory = $Objectinstance->get('\Magento\Catalog\Model\CategoryFactory');         
           $subcats = $subcategory->getChildrenCategories();
           if(count($subcats)>0):
              foreach ($subcats as $subcat) {
                    if ($subcat->getIsActive()) {
                        $subcat_url = $subcat->getUrl();
                        $subcat_img = "";
                        $placeholder_img = "/media/placeholder.png";     //$subcat->getId()               
                          echo $subcat->getName().'   <a href="'.$this->getUrl('steps/index/adsedit',array('cid'=>$subcat->getId())).'" >Edit</a><br/>'; 
                           $category = $categoryFactory->create()->load($subcat->getId());                        
                           $childrenCategories = $category->getChildrenCategories();
                           if(count($childrenCategories)>0){
                              $this->getChilds($subcat->getId());
                           }
                    }
              }
            else:
              echo 'No Kids found<br/>';
           endif;
     }
 
}