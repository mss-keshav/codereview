<?php

namespace Aghai\Steps\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
 
class Steps extends AbstractDb
{
 
    public function _construct()
    {
        $this->_init('aghai_steps', 'id');
    }
}
