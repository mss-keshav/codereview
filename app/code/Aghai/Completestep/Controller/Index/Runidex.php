<?php
 
namespace Aghai\Completestep\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
 
class Runidex extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_cart;
    public function __construct(Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, PageFactory $resultPageFactory,\Magento\Checkout\Model\Cart $_cart) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cart = $_cart;
        parent::__construct($context);
    }
 
    public function execute()
    {
      
            /* switch ($data['btnId']) {
                               case 'A':
                                   $this->addProd2($data['prod2']);
                                   $resultRedirect->setPath('checkout/cart/add',$params);
                                   break;
                               case 'B':
                                    $resultRedirect->setPath($this->_redirect->getRefererUrl());
                                   break;
                                case 'C':
                                   $this->addProd2($data['prod2']);
                                   $resultRedirect->setPath('checkout/cart/add',$params);
                                   break;
                               default:
                                   $resultRedirect->setPath('checkout/cart');
                                   break;
             }             */
            
                            
                            echo '<h1>Idex Integration</h1>'; 
                #navigate to zips directory and download zip
                 #die;
                chdir("idex/zips");
                $timestamp = time();
                $ZipFilename = 'idexzipFile_'.$timestamp.'.zip';
                $FolderToExtract = 'extractedfile_'.$timestamp.'';
                //download zip
                exec("wget 'http://idexonline.com/Idex_Feed_API-Full_Inventory?String_Access=ZELI1HYDV6Y5MXD2W1BNJVJDW' -O ".$ZipFilename.""); ## Full inventory query: http://idexonline.com/Idex_Feed_API-Full_Inventory?String_Access=ZELI1HYDV6Y5MXD2W1BNJVJDW
                                ##Inventory Update query:  http://idexonline.com/Idex_Feed_API-Inventory_Update?String_Access=ZELI1HYDV6Y5MXD2W1BNJVJDW
                chdir("..");
                chdir("..");
                #echo getcwd().'<br/>';
                mkdir('idex/xmls/'.$FolderToExtract.'',0777);
                 //extract zip 
                $zip = new \ZipArchive;
                $zip->open('idex/zips/'.$ZipFilename);
                $zip->extractTo('idex/xmls/'.$FolderToExtract);
                    for($i = 0; $i < $zip->numFiles; $i++) {
                       $UnzippedFilename =  $zip->getNameIndex($i);      
                    } 
                $zip->close();

                //parse xml
                $xml=simplexml_load_file('idex/xmls/'.$FolderToExtract.'/'.$UnzippedFilename) or die("Error: Cannot create object. Filename:".$UnzippedFilename);
                $GiaUrls = "";
                $ItemIds = "";
                foreach($xml->children() as $item) {   
                        //extract each product 
                        if($item['lab']=='GIA'&&$item['ct']>1.0) {
                            $GiaUrls[] = $item['cp'];
                            $ItemIds[] = $item['id'];
                            $Lab[] = $item['lab'];
                            $Carat[] = $item['ct'];
                            $cut[] = $item['cut'];
                            $col[] = $item['col'];
                            $cl[] = $item['cl'];
                            $cn[] = $item['cn'];
                            $ap[] = $item['ap'];
                            $tp[] = $item['tp'];
                            $pol[] = $item['pol'];
                            $sym[] = $item['sym'];
                            $mes[] = $item['mes'];
                            $dp[] = $item['dp'];
                            $tb[] = $item['tb'];
                            $cr[] = $item['cr'];
                            $pv[] = $item['pv'];
                            $gd[] = $item['gd'];
                            $cs[] = $item['cs'];
                            $fl[] = $item['fl'];
                            $cty[] = $item['cty'];
                            $st[] = $item['st'];
                            $idxl[] = $item['idxl'];
                            $nfc[] = $item['nfc'];
                            $nfci[] = $item['nfci'];
                            $nfco[] = $item['nfco'];
                            $ip[] = $item['ip'];
                        }
                } ;

                    for ($i=0; $i < count($GiaUrls) ; $i++) { 

                         if($i>10){
                            die;
                         }  
                         ################ pdf downloading code starts
                        //First Url Parse
                        $dom = new \DOMDocument;
                        $dom->recover = true;
                        $dom->strictErrorChecking = false;
                        $dom->loadHTML($this->getCurlData($GiaUrls[$i]));
                        foreach ($dom->getElementsByTagName('a') as $node) {
                          $FirstHref = $node->getAttribute('href');
                        }
                           $SecondUrl = 'http://diamondtransactions.net'.$FirstHref;
                           $dom = new \DOMDocument;
                           $dom->loadHTML($this->getCurlData($SecondUrl));
                                foreach ($dom->getElementsByTagName('iframe') as $node) {
                                  $PdfHref = $node->getAttribute('src');
                                }

                                //Download pdf file
                        $PdfFullUrl = 'http://diamondtransactions.net'.$PdfHref;
                        $PdfSavePath = "idex/pdf/GIA_".$ItemIds[$i].".pdf";
                        $ch = curl_init($PdfFullUrl);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_REFERER, 'http://www.emrada.com/');
                        $data = curl_exec($ch);
                        curl_close($ch);

                        $result = file_put_contents($PdfSavePath, $data);

                        if(!$result){
                                echo "error -> GIA_".$ItemIds[$i].".pdf  <br/>";
                        }else{
                                echo "success -> GIA_".$ItemIds[$i].".pdf  <br/>";
                        }
                        ################ pdf downloading code ends
                    }
    }

    protected function getCurlData($url){
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_HEADER, 1);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $data = curl_exec($ch);
          curl_close($ch);
        return $data;
    }
}
