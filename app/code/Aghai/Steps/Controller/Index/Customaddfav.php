<?php
namespace Aghai\Steps\Controller\Index;

class Customaddfav extends \Magento\Framework\App\Action\Action
{
    
     protected $_pageFactory;
     protected $listCompare;
     protected $wishlistRepository;
     protected $productRepository;
     public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Wishlist\Model\WishlistFactory $wishlistRepository,
     \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
      \Magento\Catalog\Model\Product\Compare\ListCompare $listCompare,
      \Magento\Framework\View\Result\PageFactory $pageFactory)
     {
          $this->_pageFactory = $pageFactory;
          $this->listCompare = $listCompare;
          $this->_wishlistRepository= $wishlistRepository;
          $this->_productRepository = $productRepository;
          return parent::__construct($context);
     }

     public function execute()
     {      
            $dataReceived = $this->getRequest()->getParams();     
            $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
            $SessionObj = $Objectinstance->create('Magento\Customer\Model\SessionFactory')->create();
            $customerSession = $Objectinstance->get('Magento\Customer\Model\Session');
            if($customerSession->isLoggedIn()){
              $customerId = $customerSession->getCustomer()->getId();
            }
           /* $this->listCompare->addProducts($SessionObj->getProd1());
            $this->listCompare->addProducts($SessionObj->getProd2());*/
            $PidArr = array($dataReceived['p1'],$dataReceived['p2']);
            foreach ($PidArr as   $value) {             
              try {
                  $product = $this->_productRepository->getById($value);
              } catch (NoSuchEntityException $e) {
                  $product = null;
              }
              $wishlist = $this->_wishlistRepository->create()->loadByCustomerId($customerId, true);
              $wishlist->addNewItem($product);
              $wishlist->save();
            }
            $this->messageManager->addSuccess(__('Products successfully Added to Favorites.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('checkout/cart');  
            return $resultRedirect;
     }
            
}