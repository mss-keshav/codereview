<?php
namespace Aghai\Steps\Block;
 
class Adsedit extends \Magento\Framework\View\Element\Template
{
    protected $_customerSession;
    protected $registry;
    protected $_productFactory;
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Checkout\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Backend\Block\Widget\Container $formCont,
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;    
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_productloader = $_productloader;
        $this->_productFactory = $productFactory;
        $this->_redirect = $redirect;
        $this->_request = $request;
        $this->registry = $registry;
        $this->formkey = $formCont;
        parent::__construct($context, $data);
    }
    
    public function _prepareLayout()
    {   $this->pageConfig->getTitle()->set(__('Ads '));
        return parent::_prepareLayout();
    }
    public function getChilds($Id){
           $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
           $subcategory = $Objectinstance->create('Magento\Catalog\Model\Category')->load($Id);  
           $categoryFactory = $Objectinstance->get('\Magento\Catalog\Model\CategoryFactory');         
           $subcats = $subcategory->getChildrenCategories();
           if(count($subcats)>0):
              foreach ($subcats as $subcat) {
                    if ($subcat->getIsActive()) {
                        $subcat_url = $subcat->getUrl();
                        $subcat_img = "";
                        $placeholder_img = "/media/placeholder.png";                    
                          echo $subcat->getId().'   <a href="'.$subcat_url.'" >'.$subcat->getName().'</a><br/>'; 

                          $category = $categoryFactory->create()->load($subcat->getId());                        
                           $childrenCategories = $category->getChildrenCategories();
                           if(count($childrenCategories)>0){
                              $this->getChilds($subcat->getId());
                           }
                    }
              }
            else:
              echo 'No Kids found<br/>';
           endif;
     }
    //catalog session  
    public function getCatalogSession() 
    { 
         return $this->_customerSession;
    }
    public function getStepssession(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->create('Magento\Customer\Model\SessionFactory')->create();
    }
    public function getProductObj()
    {
        return $this->_productFactory;
    }
    public function getStepsTable(){       
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->create('Aghai\Steps\Model\Steps'); 
    }
    //show current category ID
    public function getCatIdsss(){
        $category = $this->registry->registry('current_category');
        $return = 0;
        if($category){
           $return =   $category->getId();
        }
       return $return;
    } 
    //Collect last product
    public function getLastProduct()
    {
        $productID=$this->_checkoutSession->getLastAddedProductId(true);
        return $productID;
    }
    public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($id);
    }
    public function getCurrenturl(){
        $urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        return $urlInterface->getCurrentUrl();
    }
    public function getReferralurl(){
        #$referralUrl = $this->_resultFactory;#->_redirect->getRefererUrl();
           return   $this->_redirect->getRedirectUrl();
    }
    public function getBUrl(){
        $urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        return $urlInterface->getUrl();
    }
    public function getFormKey()
    {
        return $this->formkey->getFormKey();
    }
}
?>