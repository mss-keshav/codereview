<?php
namespace Aghai\Completestep\Block;
 
class User extends \Magento\Framework\View\Element\Template
{
public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Aghai\Completestep\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }
}