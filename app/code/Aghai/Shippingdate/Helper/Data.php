<?php
 
namespace Aghai\Shippingdate\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
   protected $scopeConfig;
   const Shipping_Date = 'shippingdate/shippingdate/shippingdate';

   public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
   {
      $this->scopeConfig = $scopeConfig;
   }

	public function getShippingDate() {
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		return $this->scopeConfig->getValue(self::Shipping_Date, $storeScope);
	}
}