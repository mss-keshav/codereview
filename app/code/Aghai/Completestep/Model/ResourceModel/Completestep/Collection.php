<?php

namespace Aghai\Completestep\Model\ResourceModel\Completestep;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Contact Resource Model Collection
 *
 * @author      Pierre FAY
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Aghai\Completestep\Model\Completestep', 'Aghai\Completestep\Model\ResourceModel\Completestep');
    }
}