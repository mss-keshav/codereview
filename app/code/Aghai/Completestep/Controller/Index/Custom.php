<?php
 
namespace Aghai\Completestep\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
 
class Custom extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_cart;
    protected $productRepository;
    public function __construct(Context $context,\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, PageFactory $resultPageFactory,\Magento\Checkout\Model\Cart $_cart) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cart = $_cart;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }
 
    public function execute()
    {
            $data = $this->getRequest()->getParams();
              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $resultRedirect = $this->resultRedirectFactory->create();
             # print_r($data); die; 
             #  Array ( [btnId] => C [box] => pink-leather [font] => signature_of_the_ancient_Rg [text] => jyoti [ringsize] => 869 [deliverydate] => 0 [remarks] => [prod1] => 1 [prod2] => 2741 [ringsizeid] => 869 [selectedAval] => 202 )
             //flush steps starts
                $SessionObj = $Objectinstance->create('Magento\Customer\Model\SessionFactory')->create();
                $SessionObj->setCurrentStep(1);
                $SessionObj->unsCcat();
                $SessionObj->unsFlag(); 
                $SessionObj->unsProd1();
                $SessionObj->unsProd2();
                $SessionObj->unsOneurl();
               // $SessionObj->unsOneprodurl() ;
                $SessionObj->unsTwourl();
                //$SessionObj->unsTwoprodurl() ;
                $SessionObj->unsLurl();
                $SessionObj->unsFlag();
                $SessionObj->unsCcat();
                $SessionObj->setActiveStep(1);
                $SessionObj->unsChangeStep();
                $SessionObj->unsselectedAval();
                $SessionObj->setRemainingBudget(3000);
                $SessionObj->setTotalBudget(3000);
                //clear magento cache
                 exec('rm -rf var/cache/*');
                 exec('rm -rf var/generation/*');
                 exec('rm -rf var/page_cache/*');
                 exec('rm -rf var/view_preprocessed/*');
                 exec('rm -rf var/report/*');
                // exec('rm -rf pub/static/*');  
                 clearstatcache();//clear php static data
                 //clear browser cache
                 header("Cache-Control: no-cache, must-revalidate");
                 header("Expires:".time()." ");
                 header("Content-Type: application/xml; charset=utf-8");
              //flush steps end
              
              /*   Array ( [uenc] => aHR0cDovLzUyLjMwLjc1LjIwOC9lbXJhZGFkZXYyLzE0ay13aGl0ZS1nb2xkLXRocmVlLXN0b25lLXN1bmJ1cnN0LXJpbmcuaHRtbA,, [product] => 285 [selected_configurable_option] => 440 [related_product] => [form_key] => EXMiSqTVAuuA448W [super_attribute] => Array ( [319] => 870 ) [checkoutbtnflag] => true )*/
                $ProductObj = $Objectinstance->create('Magento\Catalog\Model\Product');     
                $SettingProd = $ProductObj->load($data['prod1']); 
                $productTypeInstance = $SettingProd->getTypeInstance();
                $usedProducts = $productTypeInstance->getUsedProducts($SettingProd);
                $selected_configurable_option = '';
                $SelectedChildProductId = 0;
                foreach ($usedProducts  as $child) {
                    # echo '<br/> Id: '.$child->getId().'  Name: '.$child->getName().'  Option:'.$child->getData('dyo_rings_metal').'<br/>';
                             $ChildProductsIds[] = $child->getId();
                             $ChildProductsConfigOptions[] =$child->getData('dyo_rings_metal');
                             #$selected_configurable_option = $child->getId();
                             if($child->getData('dyo_rings_metal')==$data['selectedAval']){
                                $selected_configurable_option =$child->getData('dyo_rings_metal');
                                $SelectedChildProductId = $child->getId();
                             }
                          # echo 'Childs:: '.$child->getId().'  ==> '.$child->getData('dyo_rings_metal').' <==> '.$data['selectedAval'].'<br/>';
                } 
                if($SelectedChildProductId == 0){
                    $selected_configurable_option = $ChildProductsConfigOptions[0];
                    $SelectedChildProductId = $ChildProductsIds[0];
                }
                //die;$selected_configurable_option =36;
             //get products instances
               # print_r($data);die;
                  // add product 1
                  #echo 'Product1::'.$SelectedChildProductId.' Prod2ID::'.$data['prod2'].'<<<<'; die;
                //saving data into session  
                    /*[box] => pink-leather [font] => signature_of_the_ancient_Rg [text] => jyoti [ringsize] => 869*/
                  $SessionObj = $Objectinstance->create('Magento\Customer\Model\SessionFactory')->create();
                  $SessionObj->setUserbox($data['box']);
                  $SessionObj->setUserfont($data['font']);
                  $SessionObj->setUsertext($data['text']);
                  $SessionObj->setUseringsize($data['ringsize']);
                  $SessionObj->setCartprods(array($SelectedChildProductId,$data['prod2']));  
                  //saving end
                  $SettingProd ='';$StoneProd ='';                     
                          $params = array();
                          $params['qty'] = 1;
                          $Objectinstance1 = \Magento\Framework\App\ObjectManager::getInstance();  
                          $cart = $Objectinstance1->create('Magento\Checkout\Model\Cart');                 
                          $productObj2 = $Objectinstance1->create('Magento\Catalog\Model\Product');      
                          $StoneProd = $productObj2->load($SelectedChildProductId);
                          $cart->addProduct($StoneProd, $params);
                          $cart->save();   
                     
                         $resultRedirect->setPath('steps/index/prod2add?btnId='.$data['btnId'].'&prod2='.$data['prod2'].'&rand=0');
                         return $resultRedirect;
                        
    }

}
