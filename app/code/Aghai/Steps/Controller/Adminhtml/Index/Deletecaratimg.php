<?php
    namespace Aghai\Steps\Controller\Adminhtml\Index;
    class Deletecaratimg extends \Magento\Backend\App\Action{
         public function execute(){
            $data = $this->getRequest()->getParams();
            $ProductId = $data['portfolio_pid'];
            $carat = $data['carat'];
            $totalChild = 0;
            $three60 = 0;
            if(isset($data['three60']))
            {
                $three60 = 1;
            }
           
         
                    $target_dir = "caratimages/";
                    $DirPath = $target_dir.$ProductId."/".$carat;
                    if(file_exists($DirPath)){
                         shell_exec('rm -rf '.$DirPath);
                    }


              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $PortfolioModel = $Objectinstance->create('Aghai\Steps\Model\Caratimages');
              $PortfolioModel = $PortfolioModel->getCollection()->addFieldToFilter('pid', array('eq' => $ProductId))->addFieldToFilter('carat', array('eq' => $carat));
              foreach ($PortfolioModel as $port) {
                $port->delete();
              }

              $resultRedirect = $this->resultRedirectFactory->create();
              $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
              return $resultRedirect;
        }
    }
?>