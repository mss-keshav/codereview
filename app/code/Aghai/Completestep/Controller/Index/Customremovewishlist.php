<?php
 
namespace Aghai\Completestep\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
 
class Customremovewishlist extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_cart;
    protected $productRepository;
    protected $_wishlistProviderInterface;
    protected $_itemFactory;
    public function __construct(Context $context,\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,\Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProviderInterface,\Magento\Wishlist\Model\ItemFactory $itemFactory,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, PageFactory $resultPageFactory,\Magento\Checkout\Model\Cart $_cart) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cart = $_cart;
        $this->_wishlistProviderInterface = $wishlistProviderInterface;
        $this->_itemFactory = $itemFactory;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }
 
    public function execute()
    {
            $itemid = $this->getRequest()->getParam('itemid');  
            $item = $this->_itemFactory->create()->load($itemid);
                if (!$item->getId()) {
                    throw new NotFoundException(__('Item not found.'));
                }
                $wishlist = $this->_wishlistProviderInterface->getWishlist($item->getWishlistId());
                if (!$wishlist) {
                    throw new NotFoundException(__('Wishlist not found.'));
                }
                try {
                    $item->delete();
                    $wishlist->save();
                } catch (\Exception $e) {
                }              
               $resultRedirect = $this->resultRedirectFactory->create();
               //$this->addProd2($pid);             
               $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
               return $resultRedirect;
            
    }
    protected function addProd2($prod2){
                $params = array();
                $params['qty'] = 1;
                $Objectinstance1 = \Magento\Framework\App\ObjectManager::getInstance();  
                $cart = $Objectinstance1->create('Magento\Checkout\Model\Cart');                 
                $productObj2 = $Objectinstance1->create('Magento\Catalog\Model\Product');      
                $StoneProd = $productObj2->load($prod2);
                $cart->addProduct($StoneProd, $params);
                $cart->save();
    }
}
