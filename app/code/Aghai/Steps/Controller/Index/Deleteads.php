<?php
namespace Aghai\Steps\Controller\Index;

class Deleteads extends \Magento\Framework\App\Action\Action
{
    
     protected $_pageFactory;
     public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory)
     {
          $this->_pageFactory = $pageFactory;
          return parent::__construct($context);
     }

     public function execute()
     {      
          $data =  $this->getRequest()->getParams();
          $cid = $data['catid'];
          $mode = $data['mode'];
            $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
            $adsModel = $Objectinstance->create('Aghai\Steps\Model\Ads')->addAttributeToFilter('catid', $cid);
          if($mode=='all'){
              $adsModel->delete();
             # $adsModel->setCatid($data['categoryid'])->setPosition(serialize($positions))->setPics(serialize($images))->save();
          }
     }     
            
}