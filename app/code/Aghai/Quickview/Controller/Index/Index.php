<?php

namespace Aghai\Quickview\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
 
class Index extends Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_helper;

    public function __construct( Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Catalog\Helper\Product\Compare $helper, PageFactory $resultPageFactory) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
        parent::__construct($context);
    }
     
    public function execute()
    {
        $resultPage = $this->resultJsonFactory->create();
        $pid=$this->getRequest()->getPost('pid');
        if ($pid):
            echo 'Product ID '.$pid.'<br>';
/*
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->get('Magento\Catalog\Model\Product')->load($pid);

            //Product title and name
            echo $product->getTitle().'<br>';
            echo '<a href="'.$product->getProductUrl().'">'.$product->getName().'</a>'.'<br>';

            $_review=$this->_objectManager->create("Magento\Review\Model\Review");
            $_reviewFact=$this->_objectManager->get("Magento\Review\Model\ReviewFactory");
            $_reviewFact->create()->getEntitySummary($product, 1);  
            //Product rating summary
            echo $product->getRatingSummary()->getRatingSummary().'<br>';

            //Product Price
            //Product options
            $data = $product->getTypeInstance()->getConfigurableOptions($product);
            $options = array();

            foreach($data as $attr) {
              foreach($attr as $p){
                $options[$p['sku']][$p['attribute_code']] = $p['option_title'];
              }
            }
            $repository = $objectManager->create('Magento\Catalog\Model\ProductRepository');

            $config ='';
            echo '<select>';
            foreach($options as $sku =>$d) {
              $simplepr = $repository->get($sku);
              foreach($d as $k => $v) {
                echo $k.' - '.$v.' ';
                $config = $k;
                echo '<option value="'.$simplepr->getPrice().'">'.$v.'</option>';
              }
              echo ' : '.$simplepr->getPrice()."<br>";
            }
            echo '</select>';
            echo $config.'<br>';

            //Product short description
            echo $product->getData('short_description').'<br>';

            //Base image
            $imagewidth=500; $imageheight=500;
            $imageHelper  = $objectManager->get('\Magento\Catalog\Helper\Image');
            $image_url = $imageHelper->init($product, 'product_page_image_small')->setImageFile($product->getFile())->resize($imagewidth, $imageheight)->getUrl();
            echo '<img src="'.$image_url.'">'.'<br>';

            //Thumbnail
            $images = $product->getMediaGalleryImages();
            foreach($images as $child):
                echo '<img src="'.$child->getUrl().'" style="height: 100px;width: 100px;">';
            endforeach; 
            echo '<br>';

            //Favourites
            //$compareHelper = $this->_helper('Magento\Catalog\Helper\Product\Compare');
            echo '<div class="actions-secondary text-center mrg-dt" data-role="add-to-links">';
            echo '<a href="#" class="action tocompare btn btn-xl btn-block btn-link sec-col-under text-capital" data-post="'.$this->_helper->getPostDataParams($product).'" title="Save To Favourites">';
            echo '<i class="em em-heart-plus mrg-xr" aria-hidden="true"></i>Save To Favourites';
            echo '</a>';
            echo '</div>';

            //Facebook
            echo '<a href="https://www.facebook.com/sharer/sharer.php?u='.urlencode($product->getProductUrl()).'&t='.urlencode($product->getName()).'" onclick=\'javascript:window.open(this.href,"","width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes");return false\' title="Share on FaceBook">';
            echo '<i class="em em-facebook mrg-xr" aria-hidden="true"></i>Facebook</a>';

            //TWITTER    
            echo '<a href="http://twitter.com/home/?status='.urlencode($product->getProductUrl()).'('.urlencode($product->getName()).')" onclick=\'javascript:window.open(this.href,"","width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes");return false\' title="Share on Twitter">';
            echo '<i class="em em-twitter mrg-xr" aria-hidden="true"></i>Twitter</a>';

            //whatsapp
            echo '<a href="http://web.whatsapp.com/home/?status='.urlencode($product->getProductUrl()).'('.urlencode($product->getName()).')" onclick=\'javascript:window.open(this.href,"","width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes");return false\' title="Share on Whatsapp">';
            echo '<i class="em em-whatsapp mrg-xr" aria-hidden="true"></i>Whatsapp</a>';*/

        else:
            echo 'Product ID not Received.';
        endif;
    } 
}