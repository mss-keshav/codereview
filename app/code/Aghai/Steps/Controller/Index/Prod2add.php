<?php
 
namespace Aghai\Steps\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
 
class Prod2add extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_cart;
    protected $productRepository;
    public function __construct(Context $context,\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, PageFactory $resultPageFactory,\Magento\Checkout\Model\Cart $_cart) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cart = $_cart;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }
 
    public function execute()
    {
              $data = $this->getRequest()->getParams();          
              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $resultRedirect = $this->resultRedirectFactory->create();
                          $params = array();
                          $params['qty'] = 1;
                          $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();  
                          $cart = $Objectinstance->create('Magento\Checkout\Model\Cart');                 
                          $productObj = $Objectinstance->create('Magento\Catalog\Model\Product');      
                          $StoneProd = $productObj->load($data['prod2']);
                          $cart->addProduct($StoneProd, $params);
                          $cart->save();                  
     
                      # print_r($data); die;
                     
                #$resultRedirect->setPath('checkout/cart');
                switch ($data['btnId']) {
                               case 'A':
                                   $resultRedirect->setPath('checkout/cart');
                                   break;
                               case 'B':
                                   $resultRedirect->setPath($this->_redirect->getRefererUrl());
                                   break;
                                case 'C':
                                   $resultRedirect->setPath('checkout');
                                   break;
                               default:
                                   $resultRedirect->setPath('checkout/cart');
                                   break;
             }   

              return $resultRedirect;            
    }

}
