<?php
  namespace Aghai\Steps\Controller\Adminhtml\Index;

  class Import extends \Magento\Backend\App\Action
  {
    /**
    * @var \Magento\Framework\View\Result\PageFactory
    */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
         parent::__construct($context);
         $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {   if(isset($_POST['submit'])){
 
             // Count total files
             $countfiles = count($_FILES['file']['name']);

             // Looping all files
             for($i=0;$i<$countfiles;$i++){
              $filename = $_FILES['file']['name'][$i];
             
              // Upload file
              move_uploaded_file($_FILES['file']['tmp_name'][$i],'upload/'.$filename);
             
             }
        }
        return  $resultPage = $this->resultPageFactory->create();
    }
  }
?>
  