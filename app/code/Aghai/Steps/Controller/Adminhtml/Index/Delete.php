<?php
    namespace Aghai\Steps\Controller\Adminhtml\Index;
    class Delete extends \Magento\Backend\App\Action{
         public function execute(){
            $data = $this->getRequest()->getParams();
            $ProductId = $data['portfolio_pid'];
            $ChildId = $data['childId'];
            $totalChild = 0;
            $three60 = 0;
            if(isset($data['three60']))
            {
                $three60 = 1;
            }
           
         
                    $target_dir = "portfolio360images/";
                    $DirPath = $target_dir.$ProductId."/".$ChildId;
                    if(file_exists($DirPath)){
                         shell_exec('rm -rf '.$DirPath);
                    }


              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $PortfolioModel = $Objectinstance->create('Aghai\Steps\Model\Portfolio');
              $PortfolioModel = $PortfolioModel->getCollection()->addFieldToFilter('pid', array('eq' => $ProductId))->addFieldToFilter('child_id', array('eq' => $ChildId));
              foreach ($PortfolioModel as $port) {
                $port->delete();
              }

              $resultRedirect = $this->resultRedirectFactory->create();
              $resultRedirect->setPath($this->_redirect->getRefererUrl()); 
              return $resultRedirect;
        }
    }
?>