<?php

namespace Aghai\Steps\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

 
class Steps extends AbstractModel
{
    
    protected function _construct()
    {
        $this->_init(\Aghai\Steps\Model\ResourceModel\Steps::class);
    }
    
}