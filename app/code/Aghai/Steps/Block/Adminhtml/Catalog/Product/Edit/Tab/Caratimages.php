<?php
namespace Aghai\Steps\Block\Adminhtml\Catalog\Product\Edit\Tab;
 
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
 
class Caratimages extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'product/edit/caratimages.phtml';
    protected $HelperBackend;
    protected $urlBuider;
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;
 
    public function __construct(
        Context $context,\Magento\Backend\Helper\Data $HelperBackend,
        \Magento\Framework\UrlInterface $urlBuilder,
        Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        $this->HelperBackend = $HelperBackend;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }
    public function getAdminUrl()
    {
       return  $this->urlBuilder;
    }
    /**
     * Retrieve product
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }
 
}