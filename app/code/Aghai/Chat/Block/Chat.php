<?php
namespace Aghai\Chat\Block;
 
class Chat extends \Magento\Framework\View\Element\Template
{	
	public function _prepareLayout()
    {   
    	$this->pageConfig->getTitle()->set(__('Chat'));
        return parent::_prepareLayout();
    }

    public function getHelloWorldTxt()
    {
        return 'Hello world!';
    }
}