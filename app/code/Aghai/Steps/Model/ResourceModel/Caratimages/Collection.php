<?php

namespace Aghai\Steps\Model\ResourceModel\Caratimages;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
     
    public function _construct()
    {
        $this->_init('Aghai\Steps\Model\Caratimages', 'Aghai\Steps\Model\ResourceModel\Caratimages');
    }
}