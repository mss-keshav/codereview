<?php
namespace Aghai\Steps\Controller\Index;

class Dashboardnewsletter extends \Magento\Framework\App\Action\Action
{
    
     protected $_pageFactory;
     protected $subscriberFactory;
     public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
      \Magento\Framework\View\Result\PageFactory $pageFactory)
     {
          $this->_pageFactory = $pageFactory;
          $this->subscriberFactory= $subscriberFactory;
          return parent::__construct($context);
     }

     public function execute()
     {      
            $pid=0;$curcat =0;$CurrentCat=0;
            $dataReceived = $this->getRequest()->getParams();           
                    //gender                    
                $this->subscriberFactory->create()->subscribe($this->getRequest()->getParam('subsemail'));
                $this->messageManager->addSuccess(__($this->getRequest()->getParam('subsemail').' You have successfully subscribed to newsletter.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath($this->_redirect->getRefererUrl());  
                return $resultRedirect;
            
     }
    protected function isValidEmail(string $email)
    {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
 
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return false;
        }
 
        return true;
    }
            
}