<?php
namespace Aghai\Steps\Controller\Index;

class Schedulecallback extends \Magento\Framework\App\Action\Action
{
    
     protected $_pageFactory;
     protected $subscriberFactory;
     public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
      \Magento\Framework\View\Result\PageFactory $pageFactory)
     {
          $this->_pageFactory = $pageFactory;
          $this->subscriberFactory= $subscriberFactory;
          return parent::__construct($context);
     }

     public function execute()
     {      
            $pid=0;$curcat =0;$CurrentCat=0;
            $dataReceived = $this->getRequest()->getParams();     
            $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
            $Callback = $Objectinstance->create('Aghai\Steps\Model\Callback');
            $Callback->setPhone($dataReceived['phone'])->setDate($dataReceived['date'])->setTime($dataReceived['time'])->save();
            $this->messageManager->addSuccess(__('Successfully scheduled callback on '.$dataReceived['date'].' at '.$dataReceived['time'].' Phone Number:'.$dataReceived['phone']));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath($this->_redirect->getRefererUrl());  
                return $resultRedirect;
            
     }
    protected function isValidEmail(string $email)
    {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
 
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return false;
        }
 
        return true;
    }
            
}