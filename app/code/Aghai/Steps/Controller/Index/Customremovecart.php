<?php
namespace Aghai\Steps\Controller\Index;

class Customremovecart extends \Magento\Framework\App\Action\Action
{
    
     protected $_pageFactory;
     public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory)
     {
          $this->_pageFactory = $pageFactory;
          return parent::__construct($context);
     }

     public function execute()
     {       
            $dataReceived = $this->getRequest()->getParams();     
            $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
            $SessionObj = $Objectinstance->create('Magento\Customer\Model\SessionFactory')->create();
            // delete prods starts
                    $checkoutSession = $this->getCheckoutSession();
                    $allItems = $checkoutSession->getQuote()->getAllVisibleItems();//returns all teh items in session
                    foreach ($allItems as $item) {
                        $productData = $Objectinstance->get('Magento\Quote\Model\Quote\Item')->load($item->getItemId());
                        $pid = $productData->getProductId();
                       # echo $pid.'--->'.$SessionObj->getProd1().'<---'.$SessionObj->getProd2().'<br/>';
                        if($pid ==$dataReceived['p1']){
                            $itemId = $item->getItemId();//item id of particular item
                            $quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
                            $quoteItem->delete();//deletes the item
                        }
                        if($pid==$dataReceived['p2']){
                            $itemId = $item->getItemId();//item id of particular item
                            $quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
                            $quoteItem->delete();//deletes the item
                        }                       
                    }
            //delete prods ends
                $this->messageManager->addSuccess(__('Products successfully removed from cart.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath($this->_redirect->getRefererUrl());  
                return $resultRedirect; 
     }
    public function getCheckoutSession(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();//instance of object manager 
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');//checkout session
        return $checkoutSession;
    }
     
    public function getItemModel(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();//instance of object manager
        $itemModel = $objectManager->create('Magento\Quote\Model\Quote\Item');//Quote item model to load quote item
        return $itemModel;
    }
}