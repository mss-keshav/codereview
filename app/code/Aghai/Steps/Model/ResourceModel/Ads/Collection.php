<?php

namespace Aghai\Steps\Model\ResourceModel\Ads;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{ 
    public function _construct()
    {
        $this->_init('Aghai\Steps\Model\Ads', 'Aghai\Steps\Model\ResourceModel\Ads');
    }
}