<?php
 
namespace Aghai\Completestep\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
 
class Customaddwishlist extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $resultPageFactory;
    protected $_wishlistRepository;
    protected $_cart;
    protected $messageManager;
    protected $productRepository;
    public function __construct(Context $context,\Magento\Wishlist\Model\WishlistFactory $wishlistRepository,\Magento\Framework\Message\ManagerInterface $messageManager,\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, PageFactory $resultPageFactory,\Magento\Checkout\Model\Cart $_cart) 
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_cart = $_cart;        
        $this->_messageManager = $messageManager;
        $this->_wishlistRepository= $wishlistRepository;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }
 
    public function execute()
    {
            $pid = $this->getRequest()->getParam('pid');
              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $resultRedirect = $this->resultRedirectFactory->create();
              $ProductObj = $Objectinstance->create('Magento\Catalog\Model\Product');     
              $SettingProd = $ProductObj->load($pid); 
              $customerSession = $Objectinstance->get('Magento\Customer\Model\Session');
              

               if($customerSession->isLoggedIn()) {                  
                  $wishlist = $this->_wishlistRepository->create()->loadByCustomerId($customerSession->getCustomer()->getId(), true);
                  $wishlist->addNewItem($SettingProd);
                  $wishlist->save();
                  $this->messageManager->addSuccess(__('Product Added to Favorites.'));
                  $resultRedirect->setPath($this->_redirect->getRefererUrl());
               }
                
                if(!$customerSession->isLoggedIn()) {
                   $this->_messageManager->addError(__("You need to login/Signup before adding to Favorites."));
                   $resultRedirect->setPath('customer/account/login/');
                }
               return $resultRedirect;
              
            
    }
    protected function addProd2($prod2){
                $params = array();
                $params['qty'] = 1;
                $Objectinstance1 = \Magento\Framework\App\ObjectManager::getInstance();  
                $cart = $Objectinstance1->create('Magento\Checkout\Model\Cart');                 
                $productObj2 = $Objectinstance1->create('Magento\Catalog\Model\Product');      
                $StoneProd = $productObj2->load($prod2);
                $cart->addProduct($StoneProd, $params);
                $cart->save();
    }
}
