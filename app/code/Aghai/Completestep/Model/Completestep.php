<?php

namespace Aghai\Completestep\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

/**
 * Contact Model
 *
 * @author      Om Prakash
 */
class Completestep extends AbstractModel
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Aghai\Completestep\Model\ResourceModel\Completestep::class);
    }
    
}