<?php

namespace Aghai\Steps\Model\ResourceModel\Portfolio;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
     
    public function _construct()
    {
        $this->_init('Aghai\Steps\Model\Portfolio', 'Aghai\Steps\Model\ResourceModel\Portfolio');
    }
}