<?php
 
namespace Aghai\Completestep\Controller\Index;
 
use Magento\Framework\App\Action\Context;
 
class Cartuser extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
 
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_objectManager->create('Aghai\Completestep\Helper\Data')->CartItem();
        parent::__construct($context);
    }
 
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }
    public function getProductById($id)
    {
        return $this->_productRepo
            ->getById($id);
    }
}