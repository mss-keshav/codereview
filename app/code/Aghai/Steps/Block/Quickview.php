<?php
namespace Aghai\Steps\Block;
 
class Quickview extends \Magento\Framework\View\Element\Template
{
    protected $_customerSession;
    protected $registry;
    protected $_productFactory;
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Checkout\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Helper\Product $Prohelper,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;    
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_productloader = $_productloader;
        $this->_productFactory = $productFactory;
        $this->_redirect = $redirect;
        $this->_request = $request;
        $this->registry = $registry;
        $this->_Prohelper = $Prohelper;
        parent::__construct($context, $data);
    }
    
    public function _prepareLayout()
    {   $this->pageConfig->getTitle()->set(__('Quickview'));
        return parent::_prepareLayout();
    }
    //catalog session  
    public function getCatalogSession() 
    { 
         return $this->_customerSession;
    }
    public function getStepssession(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->create('Magento\Customer\Model\SessionFactory')->create();
    }
    public function getProductObj()
    {
        return $this->_productFactory;
    }
    public function getStepsTable(){       
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->create('Aghai\Steps\Model\Steps'); 
    }
    //show current category ID
    public function getCatIdsss(){
        $category = $this->registry->registry('current_category');
        $return = 0;
        if($category){
           $return =   $category->getId();
        }
       return $return;
    } 
    //Collect last product
    public function getLastProduct()
    {
        $productID=$this->_checkoutSession->getLastAddedProductId(true);
        return $productID;
    }
    public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($id);
    }
    public function getCurrenturl(){
        $urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        return $urlInterface->getCurrentUrl();
    }
    public function getReferralurl(){
        #$referralUrl = $this->_resultFactory;#->_redirect->getRefererUrl();
           return   $this->_redirect->getRedirectUrl();
    }
    public function getBUrl(){
        $urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        return $urlInterface->getUrl();
    }
}
?>