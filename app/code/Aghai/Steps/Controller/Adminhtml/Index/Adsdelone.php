<?php
    namespace Aghai\Steps\Controller\Adminhtml\Index;
    class Adsdelone extends \Magento\Backend\App\Action{
         public function execute(){
            $data = $this->getRequest()->getParams();
              $Objectinstance = \Magento\Framework\App\ObjectManager::getInstance();
              $adsModel = $Objectinstance->create('Aghai\Steps\Model\Ads');
              $collections = $adsModel->getCollection()->addFieldToFilter('entity_id', array('eq'=> $data['entityId']));
      
      
              $ExistingPositions=[];
              $ExistingPics=[];
              $ExistingFormat=[];
              $totalExistingImages=0; $i=0; 
             foreach ($collections as $collection) {
                if(count(unserialize($collection->getPics()))==1){
                  $collection->delete();
                  echo 'success';
                  exit;
                }
                if(count(unserialize($collection->getPics()))>1){
                   $position = $data['position'];
                   $ExistingPositions = unserialize($collection->getPosition());
                   $ExistingFormat = unserialize($collection->getFormat());
                   $ExistingPics = unserialize($collection->getPics());
                   unset($ExistingPositions[$position]);unset($ExistingFormat[$position]);unset($ExistingPics[$position]);
                   $collection->setPosition(serialize($ExistingPositions))->setFormat(serialize($ExistingFormat))->setPics(serialize($ExistingPics))->save();
                  echo 'success';
                  exit;
                }
             }

        }
    }
?>