<?php

namespace Aghai\Steps\Model\ResourceModel\Steps;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{ 
    public function _construct()
    {
        $this->_init('Aghai\Steps\Model\Steps', 'Aghai\Steps\Model\ResourceModel\Steps');
    }
}