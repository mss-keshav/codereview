<?php
namespace Aghai\Steps\Controller\Index;

class Address extends \Magento\Framework\App\Action\Action
{
    
     protected $_pageFactory;
     protected $customerSession;
     protected $addressRepository;
     /**
 * Sync constructor.
 * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
 */
     public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Customer\Model\Session $session,
      \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
      \Magento\Framework\View\Result\PageFactory $pageFactory
  	)
     {
          $this->_pageFactory = $pageFactory;
          $this->customerSession = $customerSession;
          $this->_session = $session;
          $this->addressRepository = $addressRepository;
          return parent::__construct($context);
     }

     public function execute()
     {  
     	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

     	$customerSession = $objectManager->get('Magento\Customer\Model\Session');

     	$customerSession->getCustomer()->getId();

 		$data = $this->getRequest()->getParams();

 		if ($this->_session->isLoggedIn()) {

 			$billingID =  $customerSession->getCustomer()->getDefaultBilling();

			$address = $objectManager->create('Magento\Customer\Model\Address')->load($billingID);

			$addressId=$address->getId();

		    $address = $this->addressRepository->getById($addressId);

		    $address->setFirstname($data['FirstName']);// Update FirstName

		    $address->setLastname($data['LastName']);// Update LastName

		    $address->setStreet(array($data['Street']));// Update Street

		    $address->setCountryId($data['Country']);// Update Country

		    $address->setPostcode($data['Postalcode']);// Update Postalcode

		    $address->setCity($data['City']);// Update City

		    $address->setRegion($data['Region']);// Update Region

		    $address->setTelephone($data['Phonenumber']);// Update Phonenumber
		    
		    $this->addressRepository->save($address);// update what ever you want

        	echo "Add Customer Address successfully";   
		} else {
		   
		//create new customer

		$url = \Magento\Framework\App\ObjectManager::getInstance();

		$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');

		$state = $objectManager->get('\Magento\Framework\App\State');

		// Customer Factory to Create Customer

		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory');

		$websiteId = $storeManager->getWebsite()->getWebsiteId();

		$store = $storeManager->getStore();

		$storeId = $store->getStoreId();

		// Instantiate object (this is the most important part)

		$customer = $customerFactory->create();

		$customer->setEmail($data['Username']);

	    $customer->setFirstname($data['FirstName']);

	    $customer->setLastname($data['LastName']);

	    $customer->setCountryId($data['Country']);

	    $customer->setPostcode($data['Postalcode']);

	    $customer->setCity($data['City']);

	    $customer->setRegion($data['Region']);

	    $customer->setTelephone($data['Phonenumber']);

	    $customer->setStreet($data['Street']);

		$customer->save();

		echo 'Create customer successfully Customer Id '.$customer->getId();
		//end create new customer
		}/*
	$resultRedirect = $this->resultRedirectFactory->create();
  	$resultRedirect->setPath($this->_redirect->getRefererUrl()); 
  	return $resultRedirect;*/
    }     
}