<?php

namespace Aghai\Steps\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Mapping extends AbstractDb
{
    public function _construct()
    {
        $this->_init('mapping_values', 'id');
    }
}