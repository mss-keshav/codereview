<?php

namespace Aghai\Steps\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

class Mapping extends AbstractModel
{
    protected $_dateTime;

    protected function _construct()
    {
        $this->_init(\Aghai\Steps\Model\ResourceModel\Mapping::class);
    }
    
}